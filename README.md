# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Quick summary
* This is the maven project which accesses social media data by REST APIs provided by Facebook,Twitter and Instagram.

* v1.0

* https://developers.facebook.com/docs/graph-api/overview

* https://dev.twitter.com/rest/public

* https://instagram.com/developer/endpoints/


### How do I get set up? ###
It just need to be built By running maven pom.xml.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

**### Restrictions ###**
**Facebook**

1.Can not share same status again . Will give Exception.
2.one photo in one API call only .
3.API Feed limit 
http://stackoverflow.com/questions/8779379/facebook-graph-api-error-feed-action-request-limit-reached
http://edoceo.com/notabene/facebook-api-oauthexception-341
4."your application may only remove posts that were created through it"	
Exception in thread "main" com.botree.social.facebook.exception.FacebookOAuthException: Received Facebook error response of type OAuthException: (#200) This post wasn't created by the application (code 200, subcode null)
5.Exception in thread "main" com.botree.social.facebook.exception.FacebookOAuthException: Received Facebook error response of type OAuthException: (#12) **events management API is deprecated for versions v2.0 and higher** (code 12, subcode null)
Can not create event .

**Instagram**
https://help.instagram.com/contact/185819881608116

**Twitter**

1.The major drawback of the Streaming API is that Twitter’s Steaming API provides only a sample of tweets that are occurring. The actual percentage of total tweets users receive with Twitter’s Streaming API varies heavily based on the criteria users request and the current traffic. Studies have estimated that using Twitter’s Streaming API users can expect to receive anywhere from 1% of the tweets to over 40% of tweets in near real-time. The reason that you do not receive all of the tweets from the Twitter Streaming API is simply because Twitter doesn’t have the current infrastructure to support it, and they don’t want to; hence, the Twitter Firehose.

2.TwitterException{exceptionCode=[2d3a38e0-1264cded], statusCode=403, message=You have already retweeted this tweet., code=327, retryAfter=-1, rateLimitStatus=null, version=4.0.1}


3.Specifies the number of tweets to try and retrieve, up to a maximum of 200 per distinct request.

4.user_timeline method can only return up to 3,200 of a user’s most recent Tweets

### Who do I talk to? ###
 Dipal Prajapati (dipal.prajapati@botreetechnologies.com)
 Personal Email ID : prajapatidipal47@yahoo.in